package com.newki.auth.router

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.android.basiclib.engine.toast.toast
import com.android.cs_service.ARouterPath
import com.newki.auth_api.router.IAuthService


@Route(path = ARouterPath.PATH_SERVICE_AUTH, name = "Auth模块路由服务")
class AuthServiceImpl : IAuthService {
    override fun isUserLogin(): Boolean {
        return false
    }
    override fun doUserLogin() {
        toast("网络请求去登录吧")
    }
    override fun init(context: Context?) {

    }
}