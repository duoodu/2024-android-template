package com.newki.auth.mvi.eis

import com.android.basiclib.base.mvi.IUIEffect
import com.android.basiclib.base.mvi.IUiIntent
import com.android.basiclib.base.mvi.IUiState
import com.google.gson.Gson
import com.newki.profile_api.entity.TopArticleBean

//Effect
sealed class AuthEffect : IUIEffect {
    data class ToastMessage(val msg: String?) : AuthEffect()
}

//Intent
sealed class AuthIntent : IUiIntent {
    data class TestGson(val gson: Gson) : AuthIntent()
    object FetchArticle : AuthIntent()
}

//State
sealed class AuthUiState : IUiState {
    object INIT : AuthUiState()
    data class SUCCESS(val article: List<TopArticleBean>) : AuthUiState()
}