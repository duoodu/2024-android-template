package com.newki.template.ui.fargment

import android.os.Bundle
import android.view.View
import com.android.basiclib.base.fragment.BaseVVDLazyLoadingFragment
import com.android.basiclib.base.vm.EmptyViewModel
import com.android.basiclib.engine.toast.toast
import com.android.basiclib.ext.click
import com.android.basiclib.utils.CommUtils
import com.android.basiclib.view.gloading.Gloading
import com.android.basiclib.view.gloading.GloadingRoatingAdapter
import com.newki.template.databinding.FragmentLoad3Binding

/**
 * 懒加载的LoadingFragment
 */
class Load3Fragment : BaseVVDLazyLoadingFragment<EmptyViewModel, FragmentLoad3Binding>() {

    companion object {
        fun obtainFragment(): Load3Fragment {
            return Load3Fragment()
        }
    }

    override fun generateGLoading(view: View): Gloading.Holder {
        return Gloading.from(GloadingRoatingAdapter()).wrap(view).withRetry {
            onGoadingRetry()
        }
    }

    override fun init(savedInstanceState: Bundle?) {

        mBinding.tvPageTitle.click {
            toast("点击标题 - 第三个Fragment")
        }

    }

    override fun onLazyInitData() {
        //模拟的Loading的情况
        showStateLoading()

        CommUtils.getHandler().postDelayed({

            showStateSuccess()

        }, 1500)
    }


}
