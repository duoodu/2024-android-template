package com.newki.template.ui.fargment

import android.os.Bundle
import com.android.basiclib.base.fragment.BaseVVDFragment
import com.android.basiclib.base.vm.EmptyViewModel
import com.android.basiclib.engine.toast.toast
import com.android.basiclib.ext.click
import com.newki.template.databinding.FragmentLoad2Binding

/**
 * 普通的Fragment
 */
class Load2Fragment : BaseVVDFragment<EmptyViewModel, FragmentLoad2Binding>() {
    companion object {
        fun obtainFragment(): Load2Fragment {
            return Load2Fragment()
        }
    }

    override fun init(savedInstanceState: Bundle?) {

        mBinding.tvPageTitle.click {
            toast("点击标题 - 第二个Fragment")
        }

    }


}
