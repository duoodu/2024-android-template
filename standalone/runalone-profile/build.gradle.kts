plugins {
    id("com.android.application")
}

// 使用自定义插件
apply<DefaultGradlePlugin>()

android {
    //需要定义 namespace 和 applicationId 的信息
    namespace = "com.runalone.profile"
    defaultConfig {
        applicationId = "com.runalone.profile"
    }

    //如果要配置 JPush、GooglePlay等配置，直接接下去写即可
}

dependencies {

    //依赖子组件
    implementation(project(":cpt-profile"))
}