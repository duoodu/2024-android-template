package com.newki.profile.router

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.android.basiclib.bean.OkResult
import com.android.cs_service.ARouterPath
import com.newki.profile.di.UserCaseDependencies
import com.newki.profile_api.entity.TopArticleBean
import com.newki.profile_api.repository.ArticleUserCase
import com.newki.profile_api.router.IProfileService
import dagger.hilt.android.EntryPointAccessors


@Route(path = ARouterPath.PATH_SERVICE_PROFILE, name = "Profile模块路由服务")
class ProfileServiceImpl : IProfileService {

    //手动在DI注入，在路由中需要使用特殊的方式注入，不能直接@Inject自动注入
    private lateinit var articleUserCase: ArticleUserCase

    override suspend fun fetchUserProfile(): OkResult<List<TopArticleBean>> {
        return articleUserCase.invoke()
    }

    override fun init(context: Context?) {
        if (context != null) {
            articleUserCase = EntryPointAccessors.fromApplication(context, UserCaseDependencies::class.java).articleUserCase()
        }
    }
}