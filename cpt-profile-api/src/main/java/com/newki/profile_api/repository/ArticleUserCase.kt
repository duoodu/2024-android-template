package com.newki.profile_api.repository

import com.android.basiclib.bean.OkResult
import com.newki.profile_api.entity.TopArticleBean
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticleUserCase @Inject constructor(
    private val repository: ProfileRepository
) {

    //唯一入口
    suspend fun invoke(): OkResult<List<TopArticleBean>> {
        //模拟一些其他特殊的逻辑，如果只是网络请求，直接在ViewModel中用Repository发起即可，这里仅为测试
        return repository.fetchTopArticle()
        //或者可以拿到数据之后做其他的操作最后返回给外部
    }

}