package com.android.basiclib.base

import android.app.Application
import android.content.Context
import android.os.Handler
import android.os.Looper
import androidx.multidex.MultiDex
import com.alibaba.android.arouter.launcher.ARouter
import com.android.basiclib.BuildConfig
import com.android.basiclib.utils.CommUtils
import com.android.basiclib.utils.NetWorkUtil
import com.android.basiclib.core.BaseLibCore
import com.android.basiclib.utils.log.MyLogUtils
import com.android.basiclib.view.web.WebViewPoolManager

/**
 * 基类的Application
 */
open class BaseApplication : Application() {

    //全局的静态Gson对象
    companion object {

        lateinit var networkType: NetWorkUtil.NetworkType   //此变量会在网络监听中被动态赋值

        //检查当前是否有网络
        fun checkHasNet(): Boolean {
            return networkType != NetWorkUtil.NetworkType.NETWORK_NO
        }

        var mCurAppVersion = "" //当前App的版本号 用于网络请求全局添加请求头

        var timeStampOffset = 0L  //设备时间与服务器时间的差值

        var isUserLogin = false   //当前用户是否登录

    }

    override fun onCreate() {
        super.onCreate()

        //获取到全局的网络状态
        networkType = NetWorkUtil.getNetworkType(this@BaseApplication.applicationContext)

        //全局的 CommUtil的初始化
        BaseLibCore.init(this, Handler(Looper.getMainLooper()), android.os.Process.myTid())

        //赋值App全局版本号
        mCurAppVersion =
            packageManager.getPackageInfo(CommUtils.getContext().packageName, 0).versionName

        //网络监听
        BaseLibCore.registerNetworkObserver(this)

        //ARouter初始化
        initRouter(this)

        //初始化WebView缓存容器
        WebViewPoolManager.prepare(this)
    }

    private fun initRouter(application: Application) {
        if (BuildConfig.DEBUG) {
            ARouter.openLog()
            ARouter.openDebug()
        }
        ARouter.init(application)
    }

    //Dex分包
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onTerminate() {
        super.onTerminate()
        BaseLibCore.unregisterNetworkObserver(this)

        //销毁WebView缓存容器
        WebViewPoolManager.destroy()
    }

    /**
     * 设置用户是否登录
     */
    protected fun setUserIsLogin(isLogin: Boolean) {
        isUserLogin = isLogin
    }

}