package com.android.basiclib.ext

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * 时间日期相关
 */


/**
 *  格式化之后的日期格式转换为时间戳字符串
 *  字符串日期格式（比如：2018-04-06)转为毫秒
 *  @param format 时间的格式，默认是按照yyyy-MM-dd HH:mm:ss来转换，如果格式不一样，则需要传入对应的格式
 */
@SuppressLint("SimpleDateFormat")
fun String.dateFormat2TimeStamp(format: String = "yyyy-MM-dd HH:mm:ss"): Long {

    return try {
        SimpleDateFormat(format).parse(this)?.time ?: 0
    } catch (e: Exception) {
        e.printStackTrace()
        0L
    }

}


// =======================  时间戳转日期 begin ↓ =========================

/**
 * 时间戳转日期
 * Long类型时间戳转为字符串的日期格式
 * @param format 时间的格式，默认是按照yyyy-MM-dd HH:mm:ss来转换，如果格式不一样，则需要传入对应的格式
 */
@SuppressLint("SimpleDateFormat")
fun Long.timeStampFormat2Date(format: String = "yyyy-MM-dd HH:mm:ss"): String {

    return try {

        //秒转毫秒
        var millisecond = this
        //转换PHP时间戳
        if (this.toString().length < 11) {
            millisecond = this * 1000
        }

        SimpleDateFormat(format).format(Date(millisecond))

    } catch (e: Exception) {
        e.printStackTrace()
        ""
    }

}

/**
 * String类型时间戳转日期
 * @param format 时间的格式，默认是按照yyyy-MM-dd HH:mm:ss来转换，如果格式不一样，则需要传入对应的格式
 */
fun String.timeStampFormat2Date(format: String = "yyyy-MM-dd HH:mm:ss"): String {
    return this.toLong().timeStampFormat2Date(format)
}

/**
 * Int类型时间戳转日期
 * @param format 时间的格式，默认是按照yyyy-MM-dd HH:mm:ss来转换，如果格式不一样，则需要传入对应的格式
 */
fun Int.timeStampFormat2Date(format: String = "yyyy-MM-dd HH:mm:ss"): String {
    return this.toLong().timeStampFormat2Date(format)
}
